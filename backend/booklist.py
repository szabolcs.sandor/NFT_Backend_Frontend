from lib2to3.pgen2.tokenize import TokenError
from flask import request, jsonify
from flask_restful import Resource
from flask_sqlalchemy import SQLAlchemy
import jwt
from functools import wraps
from user import User
from app import app

db = SQLAlchemy(app)

def token_required(func):
    @wraps(func)
    def decorator(*args, **kwargs):

        token = None

        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']

        if not token:
            return jsonify({'message': 'a valid token is missing'})

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
            current_user = User.query.filter_by(public_address=data['public_address']).first()
        except (jwt.InvalidTokenError, jwt.ExpiredSignatureError, jwt.DecodeError) as exc:
            response = jsonify(message=str(exc))
            response.status_code = 401
            return response
        return func(current_user, *args, **kwargs)
    return decorator

class Book(db.Model):
     id = db.Column(db.Integer, primary_key=True)
     title = db.Column(db.String(50), unique=True, nullable=False)
     author = db.Column(db.String(50), unique=True, nullable=False)

class BookList(Resource):
    method_decorators = {'get': [token_required]}
    
    def get(self, current_user):
        books = Book.query.all()
        result = []   

        for book in books:   
            book_data = {}               
            book_data['title'] = book.title 
            book_data['author'] = book.author
            
            result.append(book_data)   

        return jsonify({'books': result})
    
    def post(self):
        data = request.get_json() 

        new_book = Book(title=data['title'], author=data['author'])  
        db.session.add(new_book)   
        db.session.commit()   

        return jsonify({'message' : 'new book created'})