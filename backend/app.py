from random import randint
from xmlrpc.client import MAXINT
from flask import Flask, request, jsonify, make_response
from flask_restful import Api
import jwt
import datetime
from user import User, db

from web3.auto import w3
from eth_keys.exceptions import BadSignature
from eth_account.messages import encode_defunct

app = Flask(__name__)

app.config['SECRET_KEY']='Th1s1ss3cr3t'
app.config['SQLALCHEMY_DATABASE_URI']='sqlite://///home/szabolcs/code/flask_project/library.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["DEBUG"] = True

@app.route('/users/<public_address>', methods=['GET'])
def get_user(public_address):     
       
    user = User.query.filter_by(public_address=public_address).first()

    if user == None:
        return ('',204)

    return jsonify({'nonce': user.nonce})
        

@app.route('/register', methods=['POST'])
def create_user():   
   
   result = request.get_json()

   user = User(public_address=result['publicAddress'], nonce = randint(0, 1000000))
   db.session.add(user)
   db.session.commit()

   return jsonify({'nonce': user.nonce})


@app.route('/auth', methods=['POST'])
def authenticate_user():   
   
   result = request.get_json()   
   user = User.query.filter_by(public_address=result['publicAddress']).first()

   signature = result['signature']
   nonce = user.nonce
   address = user.public_address

   #Verify signature
   try:
        address_signed = w3.eth.account.recover_message(encode_defunct(text=str(nonce)), signature=signature)
        if address.lower() == address_signed.lower(): #Auth success            
            
            token = jwt.encode({'public_address' : user.public_address, 'nonce':user.nonce, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=45)}, app.config['SECRET_KEY'], "HS256")
            
            #Regenerate nonce after token creation
            user.nonce = randint(0, 1000000)
            db.session.commit()
            
            return jsonify({'token':token})
        return ('Address of signing entity not the same as issuer',401) 
   except BadSignature:
       return ('An error occured while verifying the signature',401)

from booklist import BookList
api = Api(app)
api.add_resource(BookList, '/books')

if  __name__ == '__main__':  
     app.run(debug=True)