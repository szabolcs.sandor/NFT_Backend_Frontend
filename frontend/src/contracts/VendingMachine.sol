//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

contract VendingMachine {

    // Declare state variables of the contract
    address public owner;
    mapping (address => uint) public cupcakeBalances;    


    // When 'VendingMachine' contract is deployed:
    // 1. set the deploying address as the owner of the contract
    // 2. set the deployed smart contract's cupcake balance to 100
    constructor() {
        owner = msg.sender;
        cupcakeBalances[owner] = 100;
    }

    // Allow the owner to increase the smart contract's cupcake balance
    function refill(uint amount) public {
        require(msg.sender == owner, "Only the owner can refill.");
        cupcakeBalances[owner] += amount;
    }   

    // Allow anyone to purchase cupcakes
    function purchase(uint amount) public payable {
        require(msg.value >= amount * 0.0001 ether, "You must pay at least 0.0001 ETH per cupcake");
        require(cupcakeBalances[owner] >= amount, "Not enough cupcakes in stock to complete this purchase");
        cupcakeBalances[owner] -= amount;
        cupcakeBalances[msg.sender] += amount;
    }

    function cupbalances(address account) public view returns (uint) {
        return cupcakeBalances[account];
    }

    function etherBalance(address account) public view returns (uint256) {
        return account.balance;
    }

    function withdraw(uint256 amount) public {
        require(msg.sender == owner, "Only the owner can withdraw");
        require(amount <= address(this).balance , "Balance smaller than amount requested!");
        payable(owner).transfer(amount);
    }
}
