import { pinJSONToIPFS } from "./pinata.js"
import { ethers, Contract } from "ethers";


const contractABI = require("../contract-abi.json");
const contractAddress = "0x7Fb3327d53f941264b6985efCA73352A56eb41Cb"


let provider = new ethers.providers.Web3Provider(window.ethereum)
let signer = provider.getSigner();

export const mintNFT = async (url, name, description) => {
    //error handling
    if (url.trim() == "" || name.trim() == "" || description.trim() == "") {
      return {
        success: false,
        status: "❗Please make sure all fields are completed before minting.",
      }
    }
  
    //make metadata
    const metadata = new Object()
    metadata.name = name
    metadata.image = url
    metadata.description = description
  
    //make pinata call
    const pinataResponse = await pinJSONToIPFS(metadata)
    if (!pinataResponse.success) {
      return {
        success: false,
        status: "😢 Something went wrong while uploading your tokenURI.",
      }
    }
    const tokenURI = pinataResponse.pinataUrl              
    const contract  = new Contract(contractAddress, contractABI, signer);    
        
    try {        
        const transaction = await contract.mintNFT(window.ethereum.selectedAddress, tokenURI)        
        return {
            success: true,
            status:
                "✅ Check out your transaction on Etherscan: https://goerli.etherscan.io/tx/" + transaction.hash,
        }
    } catch (error) {
        return {
            success: false,
            status: "😥 Something went wrong: " + error.message,
        }
    }
 }

export const logoutWallet = async () => {
  const addressArray = await window.ethereum.request({
    method: "eth_accounts",
  });
  fetch(`http://localhost:8080/api/v1/logout`, {
      body: JSON.stringify({ username: addressArray[0] }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    }).then(response => response.json());    
}

export const connectWallet = async () => {
    if (window.ethereum) {
      try {
        const addressArray = await window.ethereum.request({
          method: "eth_requestAccounts",
        })
        const obj = {
          status: "👆🏽 Write a message in the text-field above.",
          address: addressArray[0],
        }
        
        fetch(`http://localhost:8080/api/v1/users/${addressArray[0]}`)
        .then(response => response.status == 200 ? response.json() : '')
        .then(user => (user ? user.nonce : handleSignup(addressArray[0])))        
        .then(data => handleSignMessage(data))
        // Send signature to backend on the /auth route
        .then(signature => handleAuthenticate(addressArray[0], signature))
        .then(response => {
          //response.token 
        })  

        return obj
      } catch (err) {
        return {
          address: "",
          status: "😥 " + err.message,
        }
      }
    } else {
      return {
        address: "",
        status: (
          <span>
            <p>
              {" "}
              🦊 <a target="_blank" href={`https://metamask.io/download.html`}>
                You must install MetaMask, a virtual Ethereum wallet, in your browser.
              </a>
            </p>
          </span>
        ),
      }
    }
  }

  export const getCurrentWalletConnected = async () => {
    if (window.ethereum) {
      try {
        const addressArray = await window.ethereum.request({
          method: "eth_accounts",
        })
        if (addressArray.length > 0) {
          return {
            address: addressArray[0],
            status: "👆🏽 Write a message in the text-field above.",
          }
        } else {
          return {
            address: "",
            status: "🦊 Connect to MetaMask using the top right button.",
          }
        }
      } catch (err) {
        return {
          address: "",
          status: "😥 " + err.message,
        }
      }
    } else {
      return {
        address: "",
        status: (
          <span>
            <p>
              {" "}
              🦊 <a target="_blank" href={`https://metamask.io/download.html`}>
                You must install MetaMask, a virtual Ethereum wallet, in your browser.
              </a>
            </p>
          </span>
        ),
      }
    }
  }  

  
  export const handleSignup = async (publicAddress) => {    
    fetch(`http://localhost:8080/api/v1/users/`, {
      body: JSON.stringify({ username: publicAddress }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    }).then(response => response.json());    
  }

  
  export const handleSignMessage = async (nonce) => {
    return signer.signMessage(`${nonce}`)    
  };

  
  export const handleAuthenticate = async (publicAddress, signature) =>
    fetch(`http://localhost:8080/api/v1/users/signin`, {
      body: JSON.stringify({ username: publicAddress, signature }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    }).then(response => response.json());
